window.addEventListener('DOMContentLoaded', async () => {
    const url = 'http://localhost:8000/api/states/';

    try {
        const response = await fetch(url);

        if(!response.ok){
            throw new Error('Response not ok')
        } else {
            const data = await response.json();

            const selectTag = document.getElementById('state')
            for(let state of data.states){
                const option = document.createElement('option');

                option.innerHTML = state.name
                option.setAttribute('value', state.abbreviation)
                
                selectTag.appendChild(option)
            }
        }
        const formTag = document.getElementById('create-location-form');
        formTag.addEventListener('submit', async event => {
            event.preventDefault();
            const formData = new FormData(formTag);
            const json = JSON.stringify(Object.fromEntries(formData));

            const locationUrl = 'http://localhost:8000/api/locations/';
            const fetchConfig = {
                method: "post",
                body: json,
                headers: {
                    'Content-Type': 'application/json',
                },
            };
            const response = await fetch(locationUrl, fetchConfig);
            if (response.ok) {
                formTag.reset();
                const responseJson = await response.json();
            }
        });

    } catch (e) {
        console.error(e);
        //some error thing
    }

    

    
});