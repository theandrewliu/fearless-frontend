function createCard(name, description, pictureUrl, starts_formatted, ends_formatted,location){
    return `
    <div class ="col">
        <div class="card shadow p-3 mb-5 bg-body rounded">
            <img src = "${pictureUrl}" class = "card-img-top">
            <div class="card-body">
                <h5 class="card-title">${name}</h5>
                <h6 class="card-subtitle mb-2 text-muted">${location}</h6>
                <p class="card-text">${description}</p>
            </div>
            <div class = "card-footer">
                <small>${starts_formatted}-${ends_formatted}</small>
            </div>
        </div>
    </div>
    `
}

function error(){
    return`
    <div class="alert alert-warning d-flex align-items-center" role="alert">
        <svg class="bi flex-shrink-0 me-2" width="24" height="24" role="img" aria-label="Warning:"><use xlink:href="#exclamation-triangle-fill"/></svg>
        <div>
            Error Alert
        </div>
    </div>
    `
}

window.addEventListener('DOMContentLoaded', async () => {

    const url = 'http://localhost:8000/api/conferences/';

    try {
        const response = await fetch(url);

        if (!response.ok) {
            // Figure out what to do wwhen the response is bad
            throw new Error('Response not ok')
        } else {
            const data = await response.json();
            for(let conference of data.conferences){
                const detailURL = `http://localhost:8000${conference.href}`;
                const detailResponse = await fetch(detailURL);
                if (detailResponse.ok) {
                    const details = await detailResponse.json();
                    const name = details.conference.name;
                    const description = details.conference.description;
                    const pictureUrl = details.conference.location.picture_url;

                    const starts = details.conference.starts;
                    const dateStarts = new Date(starts);
                    const starts_formatted = `${dateStarts.getMonth()}/${dateStarts.getDate()}/${dateStarts.getFullYear()}`;
                    
                    const ends = details.conference.ends
                    const dateEnds = new Date(ends);
                    const ends_formatted = `${dateEnds.getMonth()}/${dateEnds.getDate()}/${dateEnds.getFullYear()}`;

                    const location = details.conference.location.name;
                    
                    const html = createCard(name, description, pictureUrl, starts_formatted, ends_formatted, location);
                    const column = document.querySelector('.row');
                    column.innerHTML += html;
                    // console.log(details)
                }
            }
        }
    } catch (e) {
        console.error(e);
        // Figure out what to do if an error is raised
        const myerror = error()
        const errorlocation = document.querySelector('body')
        errorlocation.innerHTML += myerror
    }

});
