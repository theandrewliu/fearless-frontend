window.addEventListener('DOMContentLoaded', async () => {
    const url = 'http://localhost:8000/api/locations'

    try {
        const response = await fetch(url);

        if(!response.ok){
            throw new Error('Response not ok')
        } else {
            const data = await response.json();

            const selectTag = document.getElementById('location')
            for(let location of data.locations){
                const option = document.createElement('option');

                option.innerHTML = location.name
                option.value = location.id

                selectTag.appendChild(option)
            }
        
        const formTag = document.getElementById('create-location-form');
        formTag.addEventListener('submit', async event => {
            event.preventDefault();
            const formData = new FormData(formTag);
            const json = JSON.stringify(Object.fromEntries(formData));

            const ConferenceUrl = 'http://localhost:8000/api/conferences/';
            const fetchConfig = {
                method: "post",
                body: json,
                headers: {
                    'Content-Type': 'application/json',
                },
            };
            const response = await fetch(ConferenceUrl, fetchConfig);
            if (response.ok) {
                formTag.reset();
                const responseJson = await response.json();
            }
        })
        }
    }catch(e){
        console.error(e);
    }
})